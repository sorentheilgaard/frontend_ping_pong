import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { Player } from './models/player.model';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {
  url:string = "https://uinames.com/api/?region=denmark";

  _players: BehaviorSubject<Player[]>;
  dataStore: {
    players: Player[]
  };

  constructor(private http : HttpClient ) {
    this.dataStore = {players: []};
    this._players = <BehaviorSubject<Player[]>> new BehaviorSubject([]);
  }

  get players() {
    return this._players.asObservable();
  }

  loadPlayer() {
    this.http.get<Player>(this.url).subscribe(data => {
      data.wins = Math.floor(Math.random() * 100) + 0;
      data.losses = Math.floor(Math.random() * 100) + 0;
      data.rating = Math.floor(Math.random() * 1200) + 1100;
      this.dataStore.players.push(data);
      this._players.next(Object.assign({}, this.dataStore).players);
    },error => console.log(error));
  }
}
