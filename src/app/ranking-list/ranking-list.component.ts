import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Player } from '../models/player.model';
import { PlayerService } from '../player.service';

@Component({
  selector: 'ranking-list',
  templateUrl: './ranking-list.component.html',
  styleUrls: ['./ranking-list.component.scss']
})
export class RankingListComponent implements OnInit {
  players: Observable<Player[]>;

  constructor(private playerService: PlayerService) { }

  ngOnInit(): void {
    for (let index = 0; index < 10; index++) {
      this.players = this.playerService.players;
      this.playerService.loadPlayer();
    }
  }
}
