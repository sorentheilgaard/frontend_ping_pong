export interface Player {
  name:string;
  surname:string;
  rating: number;
  wins:number;
  losses:number;
}
